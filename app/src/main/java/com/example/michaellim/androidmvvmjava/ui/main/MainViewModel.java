package com.example.michaellim.androidmvvmjava.ui.main;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.example.michaellim.androidmvvmjava.ui.model.MessageModel;

public class MainViewModel extends ViewModel {

    MutableLiveData<MessageModel> text;

    public MutableLiveData<MessageModel> getCurrentName() {
        if (text == null) {
            text = new MutableLiveData<MessageModel>();
        }
        return text;
    }

}
