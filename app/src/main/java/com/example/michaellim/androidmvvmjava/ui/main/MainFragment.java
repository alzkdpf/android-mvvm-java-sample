package com.example.michaellim.androidmvvmjava.ui.main;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.michaellim.androidmvvmjava.R;
import com.example.michaellim.androidmvvmjava.databinding.MainFragmentBinding;
import com.example.michaellim.androidmvvmjava.ui.model.MessageModel;

public class MainFragment extends Fragment {

    private MainViewModel mViewModel;
    private MainFragmentBinding binding;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater,
                R.layout.main_fragment, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final Observer<MessageModel> nameObserver = new Observer<MessageModel>() {
            @Override
            public void onChanged(@Nullable final MessageModel newName) {
                // Update the UI, in this case, a TextView.
                newName.setText("show 99~!!");
            }
        };

        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        mViewModel.getCurrentName().observe(this, nameObserver);

        MessageModel presenter = new MessageModel();
        presenter.setText("이제 되는 건가요??");
        binding.setPresenter(presenter);
    }

}
