package com.example.michaellim.androidmvvmjava.ui.model;

public class MessageModel {

    private String text;

    public void setText(String text){
        this.text = text;
    }

    public String getText(){
        return this.text;
    }
}
